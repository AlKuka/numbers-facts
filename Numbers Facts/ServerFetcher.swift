//
//  ServerFetcher.swift
//  Numbers Facts
//
//  Created by Oleg Levkutnyk on 29.11.2022.
//

import Foundation

public struct JsonAnswer: Decodable {
    let text : String?
    let found: Bool
    let number : Int?
}

class ServerFetcher {
    
    class func getInfo(number: Int?, result: @escaping (JsonAnswer?) -> Void) {
        let domain = "http://numbersapi.com"
        let address: String
        if number != nil {
            address = domain + "/\(number!)"
        } else {
            address = domain + "/random/math"
        }
        
        guard let url = URL(string: address) else {
            result(nil)
            return
        }
        
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                if error != nil || response == nil {
                    result(nil)
                } else {
                    if data != nil,
                       let answer = try? JSONDecoder().decode(JsonAnswer.self, from: data!) {
                        result(answer)
                    } else if data != nil,
                              let text = String(data: data!, encoding: .utf8),
                              let i = text.firstIndex(of: " ") {
                              let n = String(text[..<i])
//                        print(text)
                        let json = JsonAnswer(text: text, found: true, number: Int(n))
                        result(json)
                    } else {
                        result(nil)
                    }
                }
            }
        })
            
        task.resume()
    }
}
