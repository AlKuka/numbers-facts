//
//  ViewController.swift
//  Numbers Facts
//
//  Created by Oleg Levkutnyk on 29.11.2022.
//

import UIKit

class MainScreen: UIViewController {

    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var historyTable: UITableView!
    
    private var facts: [Fact]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        facts = getStoredFacts(managedObjectContext)
        historyTable.reloadData()
        if facts == nil {
            numberField.becomeFirstResponder()
        }
    }

    @IBAction func getRundomFact(_ sender: UIButton) {
        performSegue(withIdentifier: "infoSegue", sender: nil)
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoSegue",
           let vc = segue.destination as? InfoScreen {
            if let number = sender as? Int {
                vc.number = number
            } else if let text = sender as? String {
                vc.storedText = text
            }
        }
    }

    
    //MARK: - закрытие клавиатуры
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
}


//MARK: - TextField Delegate
extension MainScreen: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        guard numberField.text != nil && numberField.text != "" else { return true }
        performSegue(withIdentifier: "infoSegue", sender: Int(numberField.text!))
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard numberField.text != nil && numberField.text != "" else { return }
        performSegue(withIdentifier: "infoSegue", sender: Int(numberField.text!))
    }
}


//MARK: - TableView
extension MainScreen: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storedText = facts![indexPath.row].info
        performSegue(withIdentifier: "infoSegue", sender: storedText)
    }
}

extension MainScreen: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFact", for: indexPath)
        cell.textLabel?.text = facts![indexPath.row].info
        return cell
    }
}

