//
//  CoreDataHelper.swift
//  Numbers Facts
//
//  Created by Oleg Levkutnyk on 29.11.2022.
//

import UIKit
import CoreData


let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

// MARK: - Fact Entity
/**
 функция сохраняет факт про число
 - Parameters:
    - moc: managedObjectContext
    - number: число
    - text: интересный факт про указанное число
 */
public func saveFact(_ moc: NSManagedObjectContext, number: Int, text: String) {
    let fact : Fact
    //проверяем что уже сохранён этот инструмент
    if let oldFact = getFact(moc, number: number){
        fact = oldFact
    } else {
        fact = Fact(context: moc)
        fact.number = Int64(number)
        fact.info = text
    }
}


/**
 функция ищет в базе данных, и если есть - возвращает факт про число
 - Parameters:
    - moc: managedObjectContext
    - number: число
 - Returns: возвращает сущность Fact или nil
 */
public func getFact(_ moc: NSManagedObjectContext, number: Int) -> Fact? {
    let request = Fact.fetchRequest()
    request.predicate = NSPredicate(format: "number == %i", number)
    if let result = try? moc.fetch(request), result.count > 0 {
        return result[0]
    }else{
        return nil
    }
}


/**
 функция ищет в базе данных, и если есть - возвращает сохранённые факты про число
 - Parameters:
    - moc: managedObjectContext
 - Returns: возвращает cохранённые факты про число
 */
public func getStoredFacts(_ moc: NSManagedObjectContext) -> [Fact]? {
    let request = Fact.fetchRequest()
    request.sortDescriptors = [NSSortDescriptor(key: "number", ascending: true)]
    if let result = try? moc.fetch(request), result.count > 0 {
        return result
    }else{
        return nil
    }
}
