//
//  InfoScreen.swift
//  Numbers Facts
//
//  Created by Oleg Levkutnyk on 29.11.2022.
//

import UIKit

class InfoScreen: UIViewController {

    @IBOutlet weak var factText: UITextView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var number: Int?
    var storedText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if storedText != nil {
            factText.text = storedText!
        } else {
            getFact()
        }
    }
    
    private func getFact() {
        factText.text = "...please wait"
        indicator.startAnimating()
        
        DispatchQueue.global(qos: .utility).async { [self] in
            ServerFetcher.getInfo(number: number) { [self] result in
                indicator.stopAnimating()
                guard result != nil else {
                    factText.text = "download nothing, sorry :("
                    return
                }
                
                factText.text = result!.text
                number = result!.number
                if number != nil,
                    result?.text != nil {
                    saveFact(managedObjectContext, number: number!, text: result!.text!)
                }
            }
        }
    }

}
